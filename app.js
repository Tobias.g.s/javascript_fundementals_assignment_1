// bank elements
const bankBalanceElement = document.getElementById("bankBalance");
const takeLoanBtnElement = document.getElementById("takeLoanBtn");
const LoanBalanceElement = document.getElementById("loanBalance");
// where the payLoan btn will be propagated.
const payLoanDiv = document.getElementById("payLoanBtn");
// Work Elements
const workBalanceElement = document.getElementById("workBalance");
const workBtnElement = document.getElementById("workBtn");
const depositBtnElement = document.getElementById("depositBtn");
// computer section.
const computerDropdown = document.getElementById("computerDropdown");
// hidden until computer is selected
const computerDisplay = document.getElementById("computerDisplay");
const computerStatsElement = document.getElementById("computerStats");
let computerList;

// bank and work objects
// stores bank balance , outstanding loan balance , as well as functions for taking and paying off loans
let bank = {
    Balance: 0,
    LoanBalance: 0,
    takeLoan : () => {
        let loanAmount = parseInt(prompt("How much would you like to loan?", 0));
        if (typeof loanAmount == "number") {
            if((loanAmount > (bank.Balance * 2)) || bank.LoanBalance != 0){
                window.alert("Error: Sorry but you lack the sufficent capital for a loan of this size or have an existing loan allready.");
            }else {
                bank.Balance += loanAmount;
                bank.LoanBalance += loanAmount;
                updateBalances();
            }
        }
    },
    payLoan : () => {
        console.log("payloan invoked");
        if(job.Balance == bank.LoanBalance){
            bank.LoanBalance = 0;
            job.Balance = 0;
            updateBalances();
        }else if (job.Balance > bank.LoanBalance){
            console.log("second if statement passed");
            bank.Balance += job.Balance - bank.LoanBalance;
            bank.LoanBalance = 0;
            job.Balance = 0;
            console.log(bank.LoanBalance);
            updateBalances();
        }else{
            bank.LoanBalance -= job.Balance;
            job.Balance = 0;
            updateBalances();
        }
    }
}

let job = {
    wage: 100, // NOK duh
    Balance: 0,
    workOneDay : () => {
        job.Balance += job.wage;
        updateBalances(); 
    },
    depositToBank : () => {
        const deduction = (job.Balance * 0.1);
        if (bank.LoanBalance > 0 && deduction < bank.LoanBalance) {
            bank.Balance += job.Balance - deduction;
            bank.LoanBalance -= deduction;
            job.Balance = 0; 
            updateBalances();
            return
        } else if(bank.LoanBalance > 0){
            bank.LoanBalance -= deduction;
            job.Balance = 0;
            updateBalances();
            return
        }
        bank.Balance += job.Balance
        job.Balance = 0
        updateBalances();
    }
}

// updates the balances of the balance elements after changes.
function updateBalances(){
    bankBalanceElement.innerHTML = bank.Balance + " Kr";
    workBalanceElement.innerHTML = job.Balance  + " Kr"; 
    // checks if there is an outstanding loan balance, in which case will display payLoanBtn.
    if (bank.LoanBalance > 0) {
        LoanBalanceElement.innerHTML = `Current loan: ${bank.LoanBalance} Kr`;
        if(!payLoanDiv.hasChildNodes()){
            let payLoanBtn = createButton("payLoanBtn", "btn", "btn btn-danger", "pay off Loan");
            payLoanBtn.onclick = bank.payLoan;
            appendChild(payLoanDiv.id, payLoanBtn);
        }
    }else if(bank.LoanBalance == 0){
        LoanBalanceElement.innerHTML = " ";
        removeAllChildren(payLoanDiv.id);
    }
}

// takes in button parameters , returns a new button object.
function createButton(id, type, className, innerText){
    let btn = document.createElement(`button`);
    btn.id = id;
    btn.type = `${type}`;
    btn.className = className;
    btn.innerText = innerText;
    return btn;
}

// appends child ELEMENT to a parent with given ID.
function appendChild(parentId, childElement){
    const parent = document.getElementById(parentId);
    parent.appendChild(childElement);
}

// empties the inner html of a given element by id, removing ALL of its content.
function removeAllChildren(parentId){
    const parent = document.getElementById(parentId);
    parent.innerHTML = "";
}

async function fetchComputers(){
    try {
        const response = await fetch("https://hickory-quilled-actress.glitch.me/computers");
        const data = await response.json();
        return validate(data);
    } catch (error) {
        console.log(error);
        return error
    }
}

async function validate(data){
    // iterate over every object
    for (const object of data) {
        // get image url string.
        let imageUrl = object.image;
        // check if image URL path directs to png.
        try {
            let response = await fetch("https://hickory-quilled-actress.glitch.me/" + imageUrl);
            if (response.ok){
            }else{
                throw error
            }          
        }catch (error) {
            // if there was an error in fetching the image, that means it is wrongly written as a .jpg
            // therefore correct to .png
            object.image = imageUrl.replace(".jpg", ".png");
        }
    }
    return data;
}

// takes in list of computer objects and for each element creates a new optionElement and appends it to the computerDrowdown.
async function populateComputerList(){
    try {
        computerList = await fetchComputers();
        for (const computer of computerList) {
            let computerOptionElement = makeComputerOptionElement(computer);
            // after the object is created append it to the parent.
            appendChild(computerDropdown.id, computerOptionElement);
        }
        computerDropdown.addEventListener("change", displayComputerData);
    } catch (error) {
        
    }
}

function makeComputerOptionElement(computer){
    const option = document.createElement("option");
    option.textContent = computer.title;
    option.value = computer.id;
    return option;
}

function makeComputerStatElements(computerObject){
    // clear preexisting specs.
    removeAllChildren(computerStatsElement.id);
    // get list of specs.
    let computer = computerObject;
    for (const spec of computer.specs) {
        // for each spec, make specElement of type <p> spec-text </p> then append it to parent. 
        let specElement = document.createElement("p");
        specElement.innerText = spec;
        specElement.className = "card-text";
        appendChild(computerStatsElement.id, specElement);
    }
}

function purchaseComputer(){
    let selectedComputer = computerList[computerDropdown.value -1];
    if (selectedComputer.price > bank.Balance) {
        window.alert("You are lacking sufficent funds for that purchase!");
        return
    }
    // pay for computer, use pocket money over bank balance.
    let amountToPay = selectedComputer.price;
    bank.Balance -= amountToPay;
    window.alert(`Sick , gratz on your new ${selectedComputer.title}!`);
    updateBalances();

}

// takes in list of computers, and displays a computer object based on selected dropDownElement.
function displayComputerData(){
    let selectedComputer = computerList[computerDropdown.value -1];
    makeComputerStatElements(selectedComputer);
    computerDisplay.innerHTML = `
        <h3 class="card-header">${selectedComputer.title}</h3>
        <image class="computerImage" src="https://hickory-quilled-actress.glitch.me/${selectedComputer.image}"/>
        <p class="card-title"> Price: ${selectedComputer.price},-</p>
        <p class="card-text"> ${selectedComputer.description}</p>
    `
    let buyComputerBtn = createButton("buyComputerBtn", "btn", "btn btn-danger", "Buy Computer");
    buyComputerBtn.onclick = purchaseComputer;
    appendChild(computerDisplay.id, buyComputerBtn);
}   
// setts button onclick functions once the page is loaded.
window.onload = () => {
    // setting variables
    bankBalanceElement.innerHTML = 0 + " Kr";
    workBalanceElement.innerHTML = 0 + " Kr";
    // setting onclick functions to buttons.
    takeLoanBtnElement.onclick = bank.takeLoan;
    workBtnElement.onclick = job.workOneDay;
    depositBtnElement.onclick = job.depositToBank;

    // fetch data and populate dropdown list.
    populateComputerList();
}